#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
## @file
#  An example of simple script to run PIDCalib machinery for Run-II samples
#
#  @code
#  pid_calib2.py Pi -p MagUp -y 2015 
#  @endocode
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2017-05-05
#  
# =============================================================================
""" An example of simple script to run PIDCalib machinery for Run-II samples 

> pid_calib2.py Pi -p MagUp -y 2015 

"""
# =============================================================================
__version__ = "$Revision$"
__author__  = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__    = "2017-05-05"
__all__     = ()
# =============================================================================
import ROOT, cppyy
ROOT.PyConfig.IgnoreCommandLineOptions = True
# =============================================================================
from   Ostap.Logger import getLogger
if '__main__' == __name__ : logger = getLogger ( 'pid_calib2' )
else                      : logger = getLogger ( __name__     )
# =============================================================================
import ROOT, Ostap.Kisa 

# =============================================================================
## the actual function to fill PIDcalib histograms 
#  - it books two histogram  (3D in this case)
#  - it fill them with 'accepted' and 'rejected' events (3D in this case)
#  - update input historgams
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2014-05-10
class PARTICLE(object) :
    
    ## create the object 
    def __init__ ( self             ,
                   accepted         , ## accepted sample 
                   rejected         , ## rejected sample 
                   pbins            , ## bins in P
                   hbins            , ## bins in Eta
                   tbins            , ## bins in #tracks                   
                   cuts     = None  , ## additional cuts (if any) 
                   ## "Accept"-function 
                   acc_fun  = lambda self,data : data.pproject ( self.ha , 'nTracks : probe_Brunel_ETA : probe_Brunel_P/1000 ', '(%s)*probe_sWeight' % self.accepted ) ,
                   ## "reject"-function 
                   rej_fun  = lambda self,data : data.pproject ( self.hr , 'nTracks : probe_Brunel_ETA : probe_Brunel_P/1000 ', '(%s)*probe_sWeight' % self.rejected ) ) : 
        
        # 
        ## the heart of the whole game:   DEFINE PID CUTS! 
        # 
        self.accepted = accepted 
        self.rejected = rejected 
        
        logger.info ( "ACCEPTED: %s" % self.accepted ) 
        logger.info ( "REJECTED: %s" % self.rejected ) 
        
        # 
        ## book 3D-histograms
        #
        import ROOT
        from Ostap.PyRoUts import h3_axes
        
        self.ha  = h3_axes ( pbins , hbins , tbins , title = 'Accepted(%s)' % self.accepted ) 
        self.hr  = h3_axes ( pbins , hbins , tbins , title = 'Rejected(%s)' % self.rejected )

        self.cuts = cuts
        
        if self.cuts : ## redefine accepted/rejected 
            self.accepted = '(%s)*(%s)' % ( self.cuts , self.accepted )
            self.rejected = '(%s)*(%s)' % ( self.cuts , self.rejected )

        self.acc_fun = acc_fun
        self.rej_fun = rej_fun

    def __call__ ( self , data ) : return self.run ( data )
    
    ## The actual function to fill PIDCalib histograms
    def run ( self , data ) :        
        """The actual function to fill PIDCalib histograms
        - it fills histograms with 'accepted' and 'rejected' events (3D in this case)
        - update input historgams
        """

        #
        ## we need here ROOT and Ostap machinery!
        #
        
        #
        ## fill them:
        #
        aa = self.acc_fun ( self , data ) 
        rr = self.rej_fun ( self , data ) 
        
        ha = aa[1] 
        hr = rr[1] 
        
        #
        ## prepare the output
        #
        
        ha.SetName ( ha.GetTitle() )
        hr.SetName ( hr.GetTitle() )        
        
        return ha , hr 

# =============================================================================
## the actual function to fill PIDcalib histograms 
#  - it books two histogram  (3D in this case)
#  - it fill them with 'accepted' and 'rejected' events (3D in this case)
#  - update input historgams
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2014-05-10
class PROTON(PARTICLE) :

    def __init__ ( self             ,
                   cuts     = None  ) : 
        
        PARTICLE.__init__ (
            self     ,
            ## accepted sample 
            accepted = 'probe_Brunel_hasRich&&(probe_Brunel_MC15TuneV1_ProbNNp>0.4)' , ## ACCEPTED sample
            ## rejected sample 
            rejected = 'probe_Brunel_hasRich&&(probe_Brunel_MC15TuneV1_ProbNNp<0.4)' , ## REJECTED sample 
            ## binning in P 
            pbins    = [ 9  , 12 , 15 , 20 , 25 , 30 , 35 , 40 , 45 , 50 , 60 , 70 , 80 , 90 , 100 , 110 , 120 ] ,
            ## binning in ETA 
            hbins    = [ 2.0 , 2.25 , 2.5 , 2.75 , 3.0 , 3.25, 3.5 , 4.75 , 4.0 , 4.25 , 4.5 , 4.65 , 4.9 ] ,            
            ## binning in #tracks 
            tbins    = [ 0 , 150 , 250 , 400 , 1000 ] ,
            ## additional cuts (if any) 
            cuts     = cuts 
            ) 

        
# =============================================================================
## the actual function to fill PIDcalib histograms 
#  - it books two histogram  (3D in this case)
#  - it fill them with 'accepted' and 'rejected' events (3D in this case)
#  - update input historgams
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2014-05-10
class KAON(PARTICLE) :

    def __init__ ( self             ,
                   cuts     = None  ) : 
        
        PARTICLE.__init__ (
            self     ,
            ## accepted sample 
            accepted = 'probe_Brunel_hasRich&&(probe_Brunel_MC15TuneV1_ProbNNk>0.4)' , ## ACCEPTED sample
            ## rejected sample 
            rejected = 'probe_Brunel_hasRich&&(probe_Brunel_MC15TuneV1_ProbNNk<0.4)' , ## REJECTED sample 
            ## binning in P 
            pbins    = [ 3.2 , 6 , 9 , 12 , 15 , 20 , 25 , 30 , 35 , 40 , 45 , 50 , 60 , 70 , 80 , 90 , 100 , 110 , 120 , 150 ] ,
            ## binning in ETA 
            hbins    = [ 2.0 , 2.25 , 2.5 , 2.75 , 3.0 , 3.25, 3.5 , 4.75 , 4.0 , 4.25 , 4.5 , 4.65 , 4.9 ] ,            
            ## binning in #tracks 
            tbins    = [ 0 , 150 , 250 , 400 , 1000 ] ,
            ## additional cuts (if any) 
            cuts     = cuts 
            ) 

# =============================================================================
## the actual function to fill PIDcalib histograms 
#  - it books two histogram  (3D in this case)
#  - it fill them with 'accepted' and 'rejected' events (3D in this case)
#  - update input historgams
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2014-05-10
class PION(PARTICLE) :

    def __init__ ( self             ,
                   cuts     = None  ) : 
        
        PARTICLE.__init__ (
            self     ,
            ## accepted sample 
            accepted = 'probe_Brunel_hasRich&&(probe_Brunel_MC15TuneV1_ProbNNpi>0.4)' , ## ACCEPTED sample
            ## rejected sample 
            rejected = 'probe_Brunel_hasRich&&(probe_Brunel_MC15TuneV1_ProbNNpi<0.4)' , ## REJECTED sample 
            ## binning in P 
            pbins    = [ 3.2 , 6 , 9 , 12 , 15 , 20 , 25 , 30 , 35 , 40 , 45 , 50 , 60 , 70 , 80 , 90 , 100 , 110 , 120 , 150 ] ,
            ## binning in ETA 
            hbins    = [ 2.0 , 2.25 , 2.5 , 2.75 , 3.0 , 3.25, 3.5 , 4.75 , 4.0 , 4.25 , 4.5 , 4.65 , 4.9 ] ,            
            ## binning in #tracks 
            tbins    = [ 0 , 150 , 250 , 400 , 1000 ] ,
            ## additional cuts (if any) 
            cuts     = cuts 
            ) 


# =============================================================================
if '__main__' == __name__ :

    logger.info ( 80*'*'   )
    logger.info ( __doc__  )
    logger.info ( 80*'*'   )
    logger.info ( ' Author  : %s' %         __author__    ) 
    logger.info ( ' Version : %s' %         __version__   ) 
    logger.info ( ' Date    : %s' %         __date__      )
    logger.info ( ' Symbols : %s' %  list ( __all__     ) )
    logger.info ( 80*'*'   )

    #
    ## import function from Ostap
    #
    from   Ostap.PidCalib2 import run_pid_calib

    ## use it!
    
    ## run_pid_calib ( PROTON , args = ['-y', '2016', 'P' , '-q'] )
    ## run_pid_calib ( KAON , args = ['-y', '2016', 'K' ] )
    
    run_pid_calib ( PION , args = ['-y', '2016', 'pi' ] ) 

    logger.info ( 80*'*' )

# =============================================================================
# The END 
# =============================================================================
