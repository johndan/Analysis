#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
## @file  Ostap/PidCalib2.py
#  Module to run PIDCalib machinery from Urania project with Ostap fro RUN-II data
# =============================================================================
""" Module to run PIDCalib machinery from Urania project with Ostap fro RUN-II data
"""
# =============================================================================
__version__ = "$Revision$"
__author__  = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__    = "2011-06-07"
__all__     = (
    'makeParser'  , ## make  a parser 
    'runPidCalib' , ## run pid-calib machinery 
    )
# =============================================================================
import ROOT
from   Ostap.Logger import getLogger, setLogging 
if '__main__' == __name__ : logger = getLogger ( 'Ostap.PidCalib2' )
else                      : logger = getLogger ( __name__          )
import Ostap.ZipShelve as DBASE 

# =============================================================================
## PIDCALIB data samples
#  https://twiki.cern.ch/twiki/bin/view/LHCbPhysics/ChargedPID
samples  = {
    'pp/2015/MagUp'   : '/eos/lhcb/grid/prod/lhcb/LHCb/Collision15/PIDCALIB.ROOT/00057800/0000/' ,
    'pp/2015/MagDown' : '/eos/lhcb/grid/prod/lhcb/LHCb/Collision15/PIDCALIB.ROOT/00057802/0000/' ,
    'pp/2016/MagUp'   : '/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00056408/0000/' ,
    'pp/2016/MagDown' : '/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00056409/0000/' ,
    'pA/2016/MagDown' : '/eos/lhcb/grid/prod/lhcb/LHCb/Protonion16/PIDCALIB.ROOT/00058286/0000/' ,
    'Ap/2016/MagDown' : '/eos/lhcb/grid/prod/lhcb/LHCb/Ionproton16/PIDCALIB.ROOT/00058288/0000/',
    }

# =============================================================================
## TTree names
protons      = ( 'Lam0_P'         ,
                 'Lam0_HPT_P'     ,
                 'Lam0_VHPT_P'    , 
                 'Sigmacpp_P'     ,
                 'Sigmac0_P'      ,
                 'LbLcMu_P'       ,
                 'LbLcPi_P'       )
antiprotons  = tuple( p + 'bar' for p in protons )
kaons_minus  = ( 'DSt3Pi_KM'      , 
                 'DSt_KM'         , 
                 'DsPhi_KM'       , 
                 'Phi_KM'         )
kaons_plus   = tuple ( k.replace('_KM','_KP') for k in kaons_minus ) 
pions_minus  = ( 'DSt_PiM'        , 
                 'DSt3Pi_PiM'     , 
                 'KS_PiM'         )
pions_plus   = tuple ( pi.replace('_PiM','_PiP') for pi in pions_minus ) 
e_minus      = ( 'Jpsi_EM'        , 'B_Jpsi_EM' )
e_plus       = tuple ( e.replace('_EM','_EP') for e in e_minus ) 
muons_minus  = ( 'Jpsi_MuM'       , 
                 'Phi_MuM'        ,
                 'DsPhi_MuM'      ,
                 'B_Jpsi_MuM'     )
muons_plus   = tuple ( mu.replace('_MuM','_MuP') for mu in muons_minus )


# =============================================================================
## prepare the parser 
def make_parser () :
    """ Prepare the parser
    - oversimplified version of parser from MakePerfHistsRunRange.py script 
    """
    import argparse, os, sys 
    
    parser = argparse.ArgumentParser (
        formatter_class = argparse.RawDescriptionHelpFormatter,
        prog            = os.path.basename(sys.argv[0]),
        description     = """Make performance histograms for a given:
        a) data taking period <YEAR>        ( e.g. 2015    )
        b) magnet polarity    <MAGNET>      ( 'MagUp' or 'MagDown' or 'Both' )
        c) particle type      <PARTICLE>    ( 'K', 'P' , 'Pi' , 'e' , 'Mu'   )  
        """ , 
        epilog =
        """TO BE UPDATED!!!
        To use the 'MuonUnBiased' hadron samples for muon misID studies, one of the
        following tracks types should be used instead: \"K_MuonUnBiased\", \"Pi_MuonUnBiased\"
        or \"P_MuonUnBiased\"."""
        )
    
    ## add mandatory arguments
    parser.add_argument (
        'particle'    ,
        metavar = '<PARTICLE>'    , type=str    ,
        choices = \
        ( 'p'  , 'p+'  , 'p-'  ) + ( 'P'  , 'P+'  , 'P-'  ) + protons    + antiprotons + \
        ( 'K'  , 'K+'  , 'K-'  )                            + kaons_plus + kaons_minus + \
        ( 'pi' , 'pi+' , 'pi-' ) + ( 'Pi' , 'Pi+' , 'Pi-' ) +
        ( 'PI' , 'PI+' , 'PI-' ) + pions_plus + pions_minus + \
        ( 'e'  , 'e+'  , 'e-'  ) + ( 'E'  , 'E+'  , 'E-'  ) + e_plus     + e_minus     + \
        ( 'mu' , 'mu+' , 'mu-' ) + ( 'Mu' , 'Mu+' , 'Mu-' ) + 
        ( 'MU' , 'MU+' , 'MU-' ) + muons_plus + muons_minus , 
        help    = "Sets the particle type"     )

    parser.add_argument ( '-y' , '--year'            , 
                          metavar = '<YEAR>'         , type=int ,
                          choices = ( 2015 , 2016 )  ,                           
                          help    = "Data taking period"  )
    
    parser.add_argument ( '-x'  , '--collisions'      , default = 'pp' ,
                          metavar = '<COLLISIONS>'    , type=str       ,
                          choices = ( 'pp' , 'pA' , 'Ap' )  ,                           
                          help    = "Collision type"  )
   
    ## add the optional arguments
    parser.add_argument ( '-p' , '--polarity'             , default = 'Both' , 
                          metavar = '<MAGNET>'            , type=str ,
                          choices = ( 'MagUp' , 'MagDown' , 'Both' ) , 
                          help    = "Sets the magnet polarity"     )
    
    parser.add_argument ( '-f', '--maxfiles     ',
                          dest    = "MaxFiles"   ,
                          metavar = "<NUM>"      , type=int, default=-1 , 
                          help    = "The maximum number of calibration files to process")
    
    parser.add_argument ( '-c', '--cuts', dest='cuts', metavar='<CUTS>', default='',
                          help="""List of cuts to apply to the calibration sample
                          prior to determine the PID efficiencies, 
                          e.g. fiducial volume,  HASRICH, etc... 
                          """)
    parser.add_argument ( '-o' , '--output'       ,
                          type    = str           ,
                          default = 'PIDCALIB.db' ,
                          help = "The name of output database file" ) 
                          
    addGroup = parser.add_argument_group("further options")
    addGroup.add_argument ("-q", "--quiet", dest="verbose", action="store_false",
                           default=True,
                           help="Suppresses the printing of verbose information")
    
    return parser


# =============================================================================
## Load calibration samples 
def load_samples ( particles                                 ,
                   years      = ( '2015'    , '2016'       ) ,
                   collisions = ( 'pp'      , 'pA'  , 'Ap' ) ,
                   polarity   = ( 'MagDown' , 'MagUp'      ) ,
                   maxfiles   = -1                           ,
                   verbose    = False                        ) :
    """Load calibration samples
    """
     
    from   Ostap.Data import Data as Data
    
    maxfiles = maxfiles if 0 < maxfiles  else 1000000
    data     = {}
    for y in years :
        for c in collisions : 
            for p in polarity :
                tag  = '%s/%s/%s' % ( c , y , p )
                fdir = samples.get ( tag , None )
                if not fdir :
                    logger.warning('No data is found for Collisions="%s" , Year="%s" , Polarity="%s"' % ( c , y , p ) )
                    continue
                
                pattern = fdir + '*.pidcalib.root'
                for p in particles :
                    chain  = p + 'Tuple/DecayTree'
                    d               = Data ( chain , pattern , maxfiles = maxfiles , silent = not verbose )
                    key = '%s/%s' % ( tag , p )
                    
                    if not d :
                        logger.warning('No useful data is found for %s' % key )
                        continue
                    
                    data [ key ] = d

                    if verbose :
                        import Ostap.TreeDeco 
                        logger.info ('Loaded %s branches %s' % ( d , d.chain.branches() ) )
                        import Ostap.TFileDeco 
                        f  = d.files[0]
                        ff = ROOT.TFile.Open ( f )
                        ff.ls   ()
                        ff.Close()                            
    return data


# =============================================================================
## Run PID-calib machinery 
def run_pid_calib ( FUNC , args = [] ) :
    """ Run PID-calib procedure 
    """
    
    import sys
    vargs   = args + [ a for a in sys.argv[1:] if '--' != a ]
    
    parser  = make_parser        ()
    config  = parser.parse_args ( vargs )
    
    if config.verbose :  
        import Ostap.Line 
        logger.info ( __file__  + '\n' + Ostap.Line.line  ) 
        logger.info ( 80*'*'   )
        logger.info ( __doc__  )
        logger.info ( 80*'*'   )
        _vars   = vars ( config )
        _keys   = _vars.keys()
        _keys .sort()
        logger.info ( 'PIDCalib configuration:')
        for _k in _keys : logger.info ( '  %15s : %-s ' % ( _k , _vars[_k] ) )
        logger.info ( 80*'*'   )
        setLogging(2) 
        
    if config.verbose : from Ostap.Logger import logInfo    as useLog
    else              : from Ostap.Logger import logWarning as useLog 
        
    with useLog () :
        return pid_calib  ( FUNC , config ) 
        
# =============================================================================
## Run PID-calib machinery 
def pid_calib ( FUNC , config  ) : 
    """ Run PID-calib procedure 
    """

    polarity  =  config.polarity
    
    if 'Both' == polarity  : polarity  = [ 'MagUp'  , 'MagDown' ]
    else                   : polarity  = [ polarity ]

    year      =  config.year 

    
    particle  = config.particle
    particles = [ particle ]
    if   'P'   == particle.upper()  : particles = protons    + antiprotons
    elif 'P+'  == particle.upper()  : particles = protons 
    elif 'P-'  == particle.upper()  : particles = antiprotons 
    elif 'PI'  == particle.upper()  : particles = pions_plus + pions_minus 
    elif 'PI+' == particle.upper()  : particles = pions_plus 
    elif 'PI-' == particle.upper()  : particles = pions_minus  
    elif 'K'   == particle.upper()  : particles = kaons_plus + kaons_minus 
    elif 'K+'  == particle.upper()  : particles = kaons_plus 
    elif 'K-'  == particle.upper()  : particles = kaons_minus  
    elif 'E'   == particle.upper()  : particles = e_plus + e_minus 
    elif 'E+'  == particle.upper()  : particles = e_plus 
    elif 'E-'  == particle.upper()  : particles = e_minus  
    elif 'MU'  == particle.upper()  : particles = muons_plus + muons_minus 
    elif 'MU+' == particle.upper()  : particles = muons_plus 
    elif 'MU-' == particle.upper()  : particles = muons_minus  

    particles = list  ( particles )
    particles.sort()
    particles = tuple ( particles )
    
    if 2016 == year :
        for p in ( 'Sigmac0_P'  , 'Sigmac0_Pbar'  ,    ## there are no such samples 
                   'Sigmacpp_P' , 'Sigmacpp_Pbar' ,    ## there are no such samples 
                   'LbLcMu_P'   , 'LbLcMu_Pbar'   ,    ## these samples are buggy 
                   'LbLcPi_P'   , 'LbLcPi_Pbar'   ) :  ## these samples are buggy 
            if p in particles:
                logger.warning('Remove %s sample from the list' % p )
                particles = list  ( particles )
                particles.remove( p )
                particles = tuple ( particles )

    if config.collisions in ( 'pA' , 'Ap' ) :
        if 2016 != year :
            logger.error('There are no %s samples for % year' % ( config.collisions , year ) )
            return
        if 'MagUp' in polarity :
            polarity.remove ( 'MagUp' )
            logger.warning  ( 'Only MagDown samples are available for %s/%s' % ( config.collisions , year ) )
            
    
    year = [ year ]
    collisions = [ config.collisions ] 
    logger.info ( 'Data taking periods : %s' % year               )  
    logger.info ( 'Collisions          : %s' % collisions         )  
    logger.info ( 'Magnet polarities   : %s' % polarity           )  
    logger.info ( 'Particles           : %s' % list ( particles ) )  

    import Ostap.Kisa

    #
    ## Load PID samples
    #
    
    data = load_samples ( particles                    ,
                          years      = year            ,
                          collisions = collisions      ,
                          polarity   = polarity        ,
                          verbose    = config.verbose  ,
                          maxfiles   = config.MaxFiles )

    
    #
    ## Start processing
    #
    
    fun       = FUNC ()
    tacc      = None 
    trej      = None
    files     = set()
    
    keys = data.keys()
    keys.sort() 
    for k in keys :

        d         = data[k]
        if config.verbose : logger.info ( 'Processing  %s' % k )
        acc , rej = fun.run ( d.chain )
        
        if tacc     : tacc += acc
        else        : tacc  = acc.clone()
        
        if trej     : trej += rej
        else        : trej  = rej.clone()

        for f in d.files : files.add ( f )
        
        with DBASE.open( config.output ) as db :
            db[ k                                    ] =  acc,  rej 
            db[ k + ':data'                          ] = d       
            db[ k + ':conf'                          ] = config 
            db[ 'TOTAL_%s'        %  config.particle ] = tacc, trej
            db[ 'TOTAL_%s:keys'   %  config.particle ] = keys  
            db[ 'TOTAL_%s:files'  %  config.particle ] = files  
            db[ 'TOTAL_%s:conf'   %  config.particle ] = config 

    logger.info ( 'Processed: Data taking periods : %s' % year               )  
    logger.info ( 'Processed: Collisions          : %s' % collisions         )  
    logger.info ( 'Processed: Magnet polarities   : %s' % polarity           )  
    logger.info ( 'Processed: Particles           : %s' % list ( particles ) )  
    logger.info ( 'Processed: Keys                : %s' % keys               )  

    return data

# =============================================================================
# The END 
# =============================================================================
