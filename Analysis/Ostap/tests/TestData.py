#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# $Id$ 
# =============================================================================
## @file TestData.py
#
#  tests for simple data-access 
#
#  @author Vanya BELYAEV Ivan.Belyaeve@itep.ru
#  @date 2014-06-06
# 
#                    $Revision$
#  Last modification $Date$
#                 by $Author$
# =============================================================================
"""
tests for simple data-access 
"""
# =============================================================================
__version__ = "$Revision:"
__author__  = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__    = "2014-06-08"
__all__     = ()  ## nothing to be imported 
# =============================================================================
import ROOT,os 
import Ostap.PyRoUts 
from   Ostap.Data    import Data, DataAndLumi  
# =============================================================================
# logging 
# =============================================================================
from AnalysisPython.Logger import getLogger
if '__main__' == __name__ or '__builtin__' == __name__ : 
    logger = getLogger( 'Ostap.TestData' )
else : 
    logger = getLogger( __name__ )
# =============================================================================
logger.info('Test  Data')
# =============================================================================
# data patterns:
ganga = '/afs/cern.ch/work/i/ibelyaev/public/GANGA/workspace/ibelyaev/LocalXML'
if os.path.exists ( '/mnt/shared/VMDATA/LocalXML' ) :
    ganga = '/mnt/shared/VMDATA/LocalXML'
# =============================================================================
patterns = [
    ganga + '/690/*/output/ZC.root' , ## 2k+11,down
    ganga + '/691/*/output/ZC.root' , ## 2k+11,up
    ganga + '/692/*/output/ZC.root' , ## 2k+12,down
    ganga + '/693/*/output/ZC.root' , ## 2k+12,up
    ganga + '/708/*/output/ZC.root' , ## 2k+15,down
    ganga + '/709/*/output/ZC.root' , ## 2k+15,up
    ]
  
data = Data ( 'aZ0/Z0'   , patterns )
logger.info  ( 'DATA %s' % data     )

# =============================================================================
logger.info('Test  Data&Lumi')
# =============================================================================

data7  = DataAndLumi ( 'aZ0/Z0' , patterns[ :2])
data8  = DataAndLumi ( 'aZ0/Z0' , patterns[2:4])
data13 = DataAndLumi ( 'aZ0/Z0' , patterns[4: ])
logger.info ( 'DATA@ 7TeV %s' % data7  )
logger.info ( 'DATA@ 8TeV %s' % data8  )
logger.info ( 'DATA@13TeV %s' % data13 )


chain = data.chain
logger.info ( 'TChain %s' % chain )

hZ0 = ROOT.TH1D('hZ0','mass(mu+mu-)',140,50,120) 
chain.project( hZ0 , "mass", "50<= mass && mass <= 120 && 0<=c2dtf && c2dtf<5" )
logger.info ( hZ0.dump( 80 , 20 ) )

# =============================================================================
# The END 
# =============================================================================
