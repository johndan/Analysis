################################################################################
# Package: DaVinciTrackScaling
################################################################################
gaudi_subdir(DaVinciTrackScaling v1r6)

gaudi_depends_on_subdirs(GaudiAlg
                         Event/TrackEvent
                         Det/DetDesc)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(DaVinciTrackScaling
                 src/*.cpp
                 INCLUDE_DIRS AIDA
                 LINK_LIBRARIES LHCbMathLib TrackEvent DetDescLib)

gaudi_install_python_modules()
