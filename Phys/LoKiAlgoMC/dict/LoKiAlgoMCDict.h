// ============================================================================
#ifndef LOKI_LOKIALGOMCDICT_H
#define LOKI_LOKIALGOMCDICT_H 1

// redefined anyway in features.h by _GNU_SOURCE
#undef _XOPEN_SOURCE
#undef _POSIX_C_SOURCE

// ============================================================================
// Python must always be the first.
#ifndef __APPLE__
#   include "Python.h"
#endif // not __APPLE__

// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/AlgoDecorator.h"
#include "LoKi/LoKiAlgoMC.h"
#include "LoKi/MCMatchAlgoDicts.h"
#include "LoKi/MCTupleDicts.h"
#include "LoKi/GenTupleDicts.h"
// ============================================================================
namespace
{
  struct __Instantiations
  {
    LoKi::Interface<LoKi::AlgoMC>  m_a1 ;
    LoKi::Dicts::Alg<LoKi::AlgoMC> m_a2 ;
    //
    __Instantiations();
  } ;
}
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_LOKIALGOMCDICT_H
// ============================================================================
