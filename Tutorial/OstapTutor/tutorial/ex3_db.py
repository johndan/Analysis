#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ============================================================================
## @file ex1_db.py
#  Persistency, DB, files 
#  @author Vanya BELYAEV Ivan..Belyaev@itep.ru
#  @date   2014-12-10
#
# ============================================================================
""" Persistency, DB, files 
"""
# ============================================================================
__version__ = "$Revision$"
__author__  = "Vanya BELYAEV  Ivan.Belyaev@itep.ru"
__date__    = "2014-12-10"
# ============================================================================
import ROOT
# ============================================================================
# logging
# ============================================================================
from AnalysisPython.Logger import getLogger
if __name__  in ( '__main__' , '__builtin__' ) : logger = getLogger( 'ex3_db')
else : logger = getLogger( __name__ )
logger.info ('Data bases')
# ============================================================================
from   Ostap.PyRoUts      import *
#import Ostap.RootShelve   as     DBASE
#import Ostap.SQLiteShelve as     DBASE
import Ostap.ZipShelve    as     DBASE

## create new db
db = DBASE.open ( 'new_db' , 'c' )

## list content of DB
db.ls()

## add something to db
db['something' ] = 'some text'
db['something2'] = [1,2,3] , ROOT.TH1D(hID(),'',10,0,1)
db['something3'] = VE(1,1) , VE(2,2) , VE(3,3)  

## list content of DB
db.ls()

## get from DB
res  = db['something']
print res
res2 = db['something2']
print res2

## close DB
db.close()

## open DB as context manager
with DBASE.open('new_db','r') as db :

    db.ls()


histo = ROOT.TH1D(hID(),'historgam',10,0,1)

## simple ROOT files
with ROOT.TFile('new_file.root','recreate') as f :
    f.ls()
    f['SomeKey']       = histo
    f.ls()
    f['Subdir1/Box']   = ROOT.TBox(1,2,1,2)
    f.ls()
    f['Subdir2/Line']  = ROOT.TLine(1,2,3,4)
    f.ls()
    for key in f.keys() :
        print 'Key %s  object type %s' % ( key , type(f[key]) )
    



# ============================================================================
# The END 
# ============================================================================

    






